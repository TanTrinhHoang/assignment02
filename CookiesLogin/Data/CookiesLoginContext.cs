﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CookiesLogin.Data
{
    public class CookiesLoginContext: DbContext
    {
        public CookiesLoginContext (DbContextOptions<CookiesLoginContext> options)
            :base(options)
        {
        }

        public DbSet<CookiesLogin.Models.ApplicationUser> User { get; set; }
    }
}
