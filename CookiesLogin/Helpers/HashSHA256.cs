﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace CookiesLogin.Helpers
{
    public static class HashSHA256
    {
        public static string GetHash(string text)
        {
            //SHA256 is disposable by inheritance.
            var sha256 = SHA256.Create();
    
                // Send a sample text to hash.  
            var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(text));
                // Get the hashed string.  
            return BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
            
        }
    }   
}
