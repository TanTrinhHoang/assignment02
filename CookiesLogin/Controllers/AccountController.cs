﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CookiesLogin.Data;
using CookiesLogin.Helpers;
using CookiesLogin.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CookiesLogin.Controllers
{
    public class AccountController : Controller
    {
        private readonly CookiesLoginContext _context;

        public AccountController(CookiesLoginContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(ApplicationUser user)
        {
            if(user.Password != null)
            {
                var data = _context.User.Where(d => d.Username == user.Username && d.Password == HashSHA256.GetHash(user.Password)).SingleOrDefault();
                if(data != null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Name, data.ID.ToString())
                        //new Claim(ClaimTypes.Role, "Administrator")
                    };

                    var claimsIdentity = new ClaimsIdentity(
                claims, CookieAuthenticationDefaults.AuthenticationScheme);

                    var authProperties = new AuthenticationProperties
                    {
                    };

                    await HttpContext.SignInAsync(
                        CookieAuthenticationDefaults.AuthenticationScheme,
                        new ClaimsPrincipal(claimsIdentity),
                        authProperties);
                    return RedirectToAction("Index", "Account");
                }
            }
            return View();
        }
        //linq , lamda expression

        public IActionResult GetData()
        {
            string name = this.User.Claims.Where(p => p.Type == ClaimTypes.Name).First().Value;
            int n;
            int.TryParse(name, out n);
            var cont = _context.User.Where(c => c.ID == n).FirstOrDefault();
            return Json(cont);
        }

        [Authorize]
        public IActionResult Contact()
        {
            //ViewData["Name"] = "Tan";
            //var name = this.identity.Claims.Where(c => c.Type == ClaimTypes.Name)
            //       .Select(c => c.Value).SingleOrDefault();

            //var user = _contex.User.single(c => c.UserName == name);
            return View();
        }   

        public async Task<IActionResult> Logout()
        {
            await HttpContext.SignOutAsync(
                      CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Index", "Account");
        }
    }
}